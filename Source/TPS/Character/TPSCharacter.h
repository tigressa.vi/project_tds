// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS/FunctionLibrary/Types.h"
#include "TPS/WeaponDefault.h"
#include "TPSCharacter.generated.h"

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	ATPSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:

	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	UPROPERTY()
		UDecalComponent* CurrentCursor = nullptr;


	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;

	//Inputs
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();

	//Weapon	
	AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY()
	FWeaponInfo InfoWeapon;
	//for demo 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	//Variables

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float FindRotation;

	UPROPERTY(BlueprintReadOnly)
		float EnduranceBar = EnduranceBarMax;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float EnduranceBarMax;

	UPROPERTY()
		float EnduranceBarMin = 0;

	UPROPERTY()
		float AxisX;

	UPROPERTY()
		float AxisY;

	UPROPERTY(BlueprintReadOnly)
		float ResSpeed = 600.f;

	// Tick Func
	UFUNCTION()
		void MovementTick(float DeltaTime);

	//Functions

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeapon);

	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();

	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);

	UFUNCTION()
		void WeaponReloadEnd();

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP();

	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);

	UFUNCTION()
		void SprintControl();

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	//VirtualFunctions
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponents);

};

